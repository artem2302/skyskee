import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email = '';
  password = '';
  repeatPassword = '';
  name = {
    first: '',
    last: ''
  };
  phone = '';
  referral = '';
  rentalResponsibility = {
    name: '',
    address: '',
    driversLicense: '',
    licenseState: ''
  };

  constructor(private authService: AuthService) { }

  onSubmit(): void {
    this.authService.register({
      email: this.email,
      password: this.password,
      repeatPassword: this.repeatPassword,
      name: this.name,
      phone: this.phone,
      referral: this.referral,
      rentalResponsibility: this.rentalResponsibility
    })
      .subscribe(res => console.log(res),
        err => alert(err.error?.message || 'Error'));
  }

  ngOnInit(): void {
  }

}
