import { Component, OnInit } from '@angular/core';

import { UserService } from './../user/user.service';
import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  user: any = {};

  constructor(
    private userService: UserService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.getMyself().subscribe(res => {
      this.userService.getUser((res as any)._id)
        .subscribe(user => {
          this.user = user.model;

          this.user.skiers = [this.user.skiers[0]]
        });
    });
  }



}
