import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.authService.login({
      email: this.email,
      password: this.password
    }).subscribe(res => {
      const token = res.headers.get('Authorization').split(' ')[1];
      this.authService.saveToken(token);
      this.router.navigate(['/']);
    }, err => alert(err.error?.message || 'Error'));
  }

}
