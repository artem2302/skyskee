import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {
    constructor(private http: HttpClient) { }

    register(data) {
        return this.http.post('http://localhost:3000/auth/register', data);
    }

    login(data) {
        return this.http.post('http://localhost:3000/auth/login', data, { observe: 'response' });
    }

    public get isLoggedIn(): boolean {
        return (localStorage.getItem('token') !== null);
    }

    saveToken(token: string): void {
        localStorage.setItem('token', token);
    }

    logout(): void {
        localStorage.removeItem('token');
    }

    getMyself() {
        return this.http.get('http://localhost:3000/auth/profile');
    }

    public get getToken(): string {
        return localStorage.getItem('token');
    }
}
