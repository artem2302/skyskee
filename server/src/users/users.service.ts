import { Injectable, NotFoundException, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { User, UserDocument } from './schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) { }

    async create(data: CreateUserDto): Promise<User> {
        const user = new this.userModel(data);
        return user.save();
    }

    async all(): Promise<User[]> {
        return this.userModel.find();
    }

    @UseInterceptors(ClassSerializerInterceptor)
    async findById(id: string | Types.ObjectId, populateSkiers: boolean = false): Promise<User> {
        const query = this.userModel.findById(id);
        return populateSkiers ? query.populate('skiers') : query;
    }

    async findByEmail(email: string): Promise<User> {
        return this.userModel.findOne({ email });
    }

    async addSkier(userId: string | Types.ObjectId, skierId: string | Types.ObjectId): Promise<User> {
        return this.userModel.findByIdAndUpdate({ _id: userId }, { $push: { skiers: skierId as Types.ObjectId } });
    }
}