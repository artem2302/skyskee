import { Body, Controller, Get, Post, Req, Res, UseGuards, Param } from '@nestjs/common';
import { Request, Response } from 'express';

import { Roles } from '../common/decorators/roles.decorator';

import { UsersService } from './users.service';
import { POSITION } from './schemas/user.schema';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from './../auth/guards/roles.guard';
import { GetYourselfGuard } from './../auth/guards/get-yourself.guard';

import { CreateUserDto } from './dto/create-user.dto';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Get()
    async all(@Req() req: Request, @Res() res: Response) {
        return res.json({
            models: await this.usersService.all()
        });
    }

    @UseGuards(JwtAuthGuard, GetYourselfGuard)
    @Get(':id')
    async id(@Param('id') id: string, @Res() res: Response) {
        return res.json({
            model: await this.usersService.findById(id, true)
        });
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Post()
    @Roles(POSITION.ADMIN)
    async create(@Body() data: CreateUserDto, @Res() res: Response) {
        return {
            model: await this.usersService.create(data)
        };
    }
}
