import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User, UserSchema, UserDocument } from './schemas/user.schema';

@Module({
    imports: [MongooseModule.forFeatureAsync([{
        name: User.name,
        useFactory: () => {
            const schema = UserSchema;
            schema.pre<UserDocument>('save', function (next) {
                if (this.isModified('password')) {
                    this.password = this.hashPassword(this.password);
                }
                next();
            });
            return schema;
        }
    }])],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService]
})

export class UsersModule { }