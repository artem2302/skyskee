import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Exclude, classToPlain } from 'class-transformer';
import validator from 'validator';
import * as bcrypt from 'bcrypt';

export const POSITION = {
    ADMIN: 'admin',
    USER: 'user'
};

@Schema()
export class User {
    @Prop({
        required: true,
        trim: true,
        unique: true,
        validate: {
            validator: validator.isEmail
        }
    })
    email: string;

    @Prop(raw({
        type: {
            first: {
                type: String,
                required: true,
                trim: true,
                minlength: 2,
                maxlength: 20
            },
            last: {
                type: String,
                required: true,
                trim: true,
                minlength: 2,
                maxlength: 20
            }
        },
        required: true
    }))
    name: Record<string, any>

    @Prop({ required: true })
    @Exclude({ toPlainOnly: true })
    password: string;

    @Prop({ type: [{ type: Types.ObjectId, required: true, ref: 'Skier', default: [] }] })
    skiers: [Types.ObjectId];

    @Prop({
        required: true,
        enum: Object.values(POSITION)
    })
    position: string;

    @Prop()
    phone?: string;

    @Prop()
    referral?: string;

    @Prop(raw({
        name: String,
        address: String,
        driversLicense: String,
        licenseState: String
    }))
    rentalResponsibility?: Record<string, any>

    hashPassword: Function;
    comparePassword: Function;

    toJSON: Function;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.methods.comparePassword = function (password: string): boolean {
    return bcrypt.compareSync(password, this.password);
}

UserSchema.methods.hashPassword = function (password: string): string {
    return bcrypt.hashSync(password, 10);
}


UserSchema.methods.toJSON = function () {
    const obj = this.toObject();
    delete obj.password;
    return obj;
}

export type UserDocument = User & Document;