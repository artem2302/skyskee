import { IsString, IsNotEmpty, Length } from 'class-validator';

export class RentalResponsibilityDto {
    @IsString()
    @IsNotEmpty()
    @Length(4, 40)
    name: string;

    @IsString()
    @IsNotEmpty()
    @Length(10, 40)
    address: string;

    @IsString()
    @IsNotEmpty()
    @Length(5, 15)
    driversLicense: string;

    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    licenseState: string;
}