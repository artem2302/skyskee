import { IsString, IsEnum, IsNotEmpty } from 'class-validator';

import { RegisterDto } from '../../auth/dto/register.dto';
import { POSITION } from '../schemas/user.schema'

export class CreateUserDto extends RegisterDto {
    @IsString()
    @IsNotEmpty()
    @IsEnum(POSITION)
    position: string;
}