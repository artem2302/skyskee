import { IsString, IsNotEmpty, Length } from 'class-validator';

export class NameDto {
    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    first: string;

    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    last: string;
}