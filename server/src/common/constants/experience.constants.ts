export const EXPERIENCE = {
    BEGINNER: 'Beginner - Green Circle',
    INTERMEDIATE: 'Intermediate - Blue Square',
    ADVANCED: 'Advanced - Black Diamond',
    EXPERT: 'Expert - Double Black Diamond'
}