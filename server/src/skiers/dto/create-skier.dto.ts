import {
    IsNotEmpty,
    ValidateNested,
    IsOptional,
    IsDateString,
    IsMongoId
} from 'class-validator';

import { NameDto } from './../../users/dto/name.dto';
import { DefaultRentalInfoDto } from './default-rental-info.dto';

export class CreateSkierDto {
    @IsNotEmpty()
    @ValidateNested()
    name: NameDto;

    @IsNotEmpty()
    //@IsDateString()
    dob: Date;

    @IsOptional()
    @ValidateNested()
    @IsNotEmpty()
    defaultRentalInfo: DefaultRentalInfoDto;
    
    @IsMongoId()
    @IsOptional()
    user: string;
}
