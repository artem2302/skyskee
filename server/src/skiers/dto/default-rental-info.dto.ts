import {
    IsString,
    IsNotEmpty,
    IsInt,
    IsBoolean,
    IsEnum,
    IsNumber,
    Min,
    Max,
    IsDivisibleBy,
    IsObject,
    ValidateNested
} from 'class-validator';

import { RENTALS } from './../../common/constants/rentals.constants';
import { EXPERIENCE } from './../../common/constants/experience.constants';

class HeightDto {
    @IsNumber()
    @IsInt()
    @Min(4)
    @Max(7)
    feet: number;

    @IsNumber()
    @IsInt()
    @Min(0)
    @Max(9)
    inches: number;
}

export class DefaultRentalInfoDto {
    @IsString()
    @IsNotEmpty()
    @IsEnum(RENTALS)
    rental: string;

    @IsString()
    @IsNotEmpty()
    @IsEnum(EXPERIENCE)
    experience: string;

    @IsBoolean()
    @IsNotEmpty()
    performanceRental: boolean;

    @IsBoolean()
    @IsNotEmpty()
    rentHelmet: boolean;

    @IsNumber()
    @Min(6)
    @Max(15)
    shoeSize: number;

    @IsObject()
    @ValidateNested()
    height: HeightDto;

    @IsNumber()
    @IsDivisibleBy(1)
    @Min(90)
    @Max(300)
    weight: number;

}