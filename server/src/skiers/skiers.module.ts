import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

import { SkiersService } from './skiers.service';
import { SkiersController } from './skiers.controller';
import { Skier, SkierSchema } from './schemas/skier.schema';

import { UsersModule } from './../users/users.module';

@Module({
    imports: [
        MongooseModule.forFeature([{
            name: Skier.name,
            schema: SkierSchema
        }]),
        UsersModule
    ],
    controllers: [SkiersController],
    providers: [SkiersService],
    exports: [SkiersService]
})

export class SkiersModule { }