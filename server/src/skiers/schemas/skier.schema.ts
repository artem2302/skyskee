import { Document } from 'mongoose';
import { Schema, SchemaFactory, Prop, raw } from '@nestjs/mongoose';

@Schema()
export class Skier {
    @Prop(raw({
        type: {
            first: {
                type: String,
                required: true,
                trim: true,
                minlength: 2,
                maxlength: 20
            },
            last: {
                type: String,
                required: true,
                trim: true,
                minlength: 2,
                maxlength: 20
            }
        },
        required: true
    }))
    name: Record<string, any>

    @Prop()
    dob: Date;

    @Prop(raw({
        type: {
            rental: String,
            experience: String,
            performanceRental: Boolean,
            rentHelmet: Boolean,
            shoeSize: Number,
            height: {
                feet: Number,
                inches: Number
            },
            weight: Number
        }
    }))
    defaultRentalInfo?: Record<string, any>
}

export const SkierSchema = SchemaFactory.createForClass(Skier);
export type SkierDocument = Skier & Document;