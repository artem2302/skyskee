import { Body, Controller, Get, Post, Req, Res, UseGuards, Query, BadRequestException } from '@nestjs/common';
import { Request, Response } from 'express';

import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

import { SkiersService } from './skiers.service';

import { POSITION, UserDocument } from './../users/schemas/user.schema';

import { CreateSkierDto } from './dto/create-skier.dto';

@Controller('skiers')
@UseGuards(JwtAuthGuard)
export class SkiersController {
    constructor(private readonly skiersService: SkiersService) { }

    @Get()
    async all(@Req() req: Request, @Query('userId') userId: string) {
        if ((req.user as UserDocument).position === POSITION.ADMIN) {
            if (!userId) {
                throw new BadRequestException('User ID has to be defined');
            }
        }
        return this.skiersService.allByUser((req.user as UserDocument).position === POSITION.ADMIN ? userId : (req.user as UserDocument)._id);
    }

    @Post()
    async create(@Req() req: Request, @Body() body: CreateSkierDto) {
        if ((req.user as UserDocument).position !== POSITION.ADMIN) {
            body.user = (req.user as UserDocument)._id;
        }
        return this.skiersService.create(body);
    }

}
