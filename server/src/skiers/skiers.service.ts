import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { Skier, SkierDocument } from './schemas/skier.schema';
import { CreateSkierDto } from './dto/create-skier.dto';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class SkiersService {
    constructor(
        @InjectModel(Skier.name) private skierModel: Model<SkierDocument>,
        private usersService: UsersService,
    ) { }

    async create(data: CreateSkierDto): Promise<Skier> {
        const user = await this.usersService.findById(data.user);
        if (!user)
            throw new BadRequestException('User not found');
        let skier = new this.skierModel(data);
        skier = await skier.save();
        await this.usersService.addSkier(data.user, skier._id)
        return skier;
    }

    async allByUser(userId: string | Types.ObjectId): Promise<Skier[]> {
        const user = await this.usersService.findById(userId);
        if (!user || !user.skiers.length)
            return null;
        return this.skierModel.find({ _id: { $in: user.skiers } });
    }

    async remove(id: string | Types.ObjectId): Promise<SkierDocument> {
        return this.skierModel.findByIdAndRemove(id);
    }
}