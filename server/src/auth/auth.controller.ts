import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { Controller, Post, UseGuards, Req, Res, Body, Get, Header } from '@nestjs/common';
import { Request, Response } from 'express';

import { RegisterDto } from './dto/register.dto';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Req() req, @Res() res: Response) {
        return res
            .set('Authorization', `Bearer ${this.authService.generateToken(req.user)}`)
            .json({
                user: req.user
            });
    }

    @Post('register')
    async register(@Body() body: RegisterDto, @Res() res: Response) {
        const user = await this.authService.register(body);
        return res
            .set('Authorization', `Bearer ${this.authService.generateToken(user)}`)
            .json({
                model: user
            });
    }

    @UseGuards(JwtAuthGuard)
    @Get('profile')
    profile(@Req() req) {
        return req.user;
    }


}
