import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class GetYourselfGuard implements CanActivate {
    constructor() { }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const req = context.switchToHttp().getRequest();
        const id = req.params?.id;
        const userId = req.user?._id;
        if (id && userId.toString() === id) {
            return true;
        }
        return false;
    }
}