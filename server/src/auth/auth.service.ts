import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';

import { UsersService } from './../users/users.service';
import { User, UserDocument, POSITION } from './../users/schemas/user.schema';
import { RegisterDto } from './dto/register.dto';


@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
        @InjectModel(User.name) private userModel: Model<UserDocument>
    ) { }

    async validateUser(email: string, password: string): Promise<User> {
        const user = await this.usersService.findByEmail(email);
        if (user && user.comparePassword(password)) {
            return user;
        }
        return null;
    }

    generateToken(user: any) {
        const payload = { sub: user._id, email: user.email }
        return this.jwtService.sign(payload)
    }

    async register(data: RegisterDto): Promise<User> {
        const user = new this.userModel({
            position: POSITION.USER,
            ...data
        });
        return user.save();
    }
}
