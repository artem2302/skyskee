import {
    IsEmail,
    IsNotEmpty,
    Length,
    IsString,
    ValidateNested,
    IsOptional,
} from 'class-validator';

import { NameDto } from './../../users/dto/name.dto';
import { RentalResponsibilityDto } from './../../users/dto/rental-responsibility.dto';

export class RegisterDto {
    @IsString()
    @IsEmail()
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(6, 20)
    password: string;

    @IsNotEmpty()
    @ValidateNested()
    name: NameDto;

    @IsString()
    @IsOptional()
    @Length(5, 15)
    phone: string;

    @IsString()
    @IsOptional()
    @Length(2, 10)
    referral: string;

    @IsOptional()
    @ValidateNested()
    rentalResponsibility: RentalResponsibilityDto;
}